<div class="container">
    <div class="row">
        <div class="col-lg-6 p-5 column-landing-page d-flex align-items-center">
            <div>
                <div>
                    <div class="d-flex justify-content-center">
                        <img src="/assets/images/logo_kab_blitar.png" width="200px" alt="Logo Kabupaten Blitar"
                            class="img img-fluid">
                    </div>
                    <div class="text-center mt-4">
                        <h1>Portal Data</h1>
                        <h4>Dinas Perumahan dan Kawasan Permukiman Kabupaten Blitar</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 p-5 column-landing-page d-flex align-items-center">
            <div class="row d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="card my-2 mx-2 text-light portal-menu-item perencanaan">
                        <div class="card-body text-center">
                            <h1><i class="fa fa-list-alt" aria-hidden="true"></i></h1>
                            <b>PERENCANAAN</b>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card my-2 mx-2 text-light portal-menu-item bangkim">
                        <div class="card-body text-center">
                            <h1><i class="fa fa-cogs" aria-hidden="true"></i></h1>
                            <b>BANGKIM</b>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card my-2 mx-2 text-light portal-menu-item pertahanan">
                        <div class="card-body text-center">
                            <h1><i class="fa fa-shield" aria-hidden="true"></i></h1>
                            <b>PERTANAHAN</b>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card my-2 mx-2 text-light portal-menu-item perumahan">
                        <div class="card-body text-center">
                            <h1><i class="fa fa-home" aria-hidden="true"></i></h1>
                            <b>PERUMAHAN</b>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card my-2 mx-2 text-light portal-menu-item psu">
                        <div class="card-body text-center">
                            <h1><i class="fa fa-database" aria-hidden="true"></i></h1>
                            <b>PSU</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
