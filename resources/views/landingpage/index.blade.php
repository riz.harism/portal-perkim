@extends('layouts.landingpage.app')
@section('title', 'Home')
@section('content')
    @include('landingpage.html')
@endsection
@section('extra_javascript')
    @include('landingpage.javascript')
@endsection
