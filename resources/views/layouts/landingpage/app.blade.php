<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {{-- Google Fonts --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;700&display=swap" rel="stylesheet">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    {{-- Bootstrap CSS --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <title>
        @yield('title') | Portal Aplikasi Dinas Perumahan dan Kawasan Permukiman Kabupaten Blitar
    </title>

    <style>
        * {
            font-family: 'Poppins', sans-serif;
        }

        /* body {
            background-image: url('https://prosesnews.id/wp-content/uploads/2020/10/kantor-bupati-blitar.jpg');
            background-size: cover;
        } */

        h1,
        h2,
        h3,
        h4,
        h5 {
            font-weight: bold;
        }

        .column-landing-page {
            height: 100vh;
        }

        .portal-menu-item:hover {
            cursor: pointer;
            background-color: wheat;
            color: black !important;
            transition: all 0.5s ease-in-out;
            /* animation: bounce 0.2s ease infinite */
        }

        .perencanaan {
            background-color: #cdb4db;
        }

        .bangkim {
            background-color: #d4a373;
        }

        .pertahanan {
            background-color: #f08080;
        }

        .perumahan {
            background-color: #9381ff;
        }

        .psu {
            background-color: #60d394;
        }
    </style>
</head>

<body>

    @yield('content')

    {{-- Bootstrap --}}
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

    {{-- jQuery --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.js"
        integrity="sha512-+k1pnlgt4F1H8L7t3z95o3/KO+o78INEcXTbnoJQ/F2VqDVhWoaiVml/OEHv9HsVgxUaVW+IbiZPUJQfF/YxZw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    @yield('extra_javascript')
</body>

</html>
